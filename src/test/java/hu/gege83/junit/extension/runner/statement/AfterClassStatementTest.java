package hu.gege83.junit.extension.runner.statement;

import static org.mockito.Mockito.verify;
import hu.gege83.junit.extension.runner.ClassRunNotifier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AfterClassStatementTest {

	@Mock
	private Statement nextStatement;
	@Mock
	private ClassRunNotifier notifier;

	private AfterClassStatement underTest;

	@Before
	public void setUp() throws Exception {
		underTest = new AfterClassStatement(nextStatement, notifier);
	}

	@Test
	public void test() throws Throwable {
		// given
		// when
		underTest.evaluate();
		// then
		verify(notifier).notifyAfterClass();
	}

}
