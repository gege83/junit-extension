package hu.gege83.junit.extension.runner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import hu.gege83.junit.extension.listener.RunListenerAdapter;
import hu.gege83.junit.extension.runner.statement.AfterClassStatement;
import hu.gege83.junit.extension.runner.statement.BeforeClassStatement;
import hu.gege83.junit.extension.stub.AnnotatedTestClass;
import hu.gege83.junit.extension.stub.TestRunListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.Statement;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JunitListenerRunnerTest {

	private JunitListenerRunner underTest;
	private ClassRunNotifier classNotifier;
	private RunListener testRunListener;

	@Mock
	private Statement statement;
	@Mock
	private RunNotifier notifier;

	@Before
	public void setup() throws Exception {
		underTest = new JunitListenerRunner(AnnotatedTestClass.class);
		classNotifier = new ClassRunNotifier(AnnotatedTestClass.class);
		classNotifier.addListener(new TestRunListener());
		testRunListener = new RunListenerAdapter(classNotifier);
	}

	@Test
	public void testConstructor() throws Exception {
		// given
		ClassRunNotifier expected = classNotifier;

		// when
		ClassRunNotifier actual = underTest.getClassRunNotifier();
		// then
		assertEquals(expected, actual);
	}

	@Test
	public void testWithBeforeClasses() {
		// given
		ClassRunNotifier expected = classNotifier;
		// when
		Statement actual = underTest.withBeforeClasses(statement);
		// then
		assertTrue(actual instanceof BeforeClassStatement);
		BeforeClassStatement beforeActual = (BeforeClassStatement) actual;
		assertEquals(expected, beforeActual.getNotifier());
		assertEquals(statement, beforeActual.getNextStatement());
	}

	@Test
	public void testWithAfterClasses() {
		// given
		ClassRunNotifier expected = classNotifier;
		// when
		Statement actual = underTest.withAfterClasses(statement);
		// then
		assertTrue(actual instanceof AfterClassStatement);
		AfterClassStatement beforeActual = (AfterClassStatement) actual;
		assertEquals(expected, beforeActual.getNotifier());
		assertEquals(statement, beforeActual.getPreviousStatement());
	}

	@Test
	public void testRun() {
		// given
		// when
		underTest.run(notifier);
		// then
		verify(notifier).addListener(testRunListener);
	}
}
