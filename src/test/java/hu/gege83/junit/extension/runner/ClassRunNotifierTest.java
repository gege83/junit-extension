package hu.gege83.junit.extension.runner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import hu.gege83.junit.extension.listener.ClassRunListener;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClassRunNotifierTest {

	private ClassRunNotifier underTest;
	@Mock
	private ClassRunListener runListener1;
	@Mock
	private ClassRunListener runListener2;
	private Description description;

	@Before
	public void setup() {
		underTest = new ClassRunNotifier(getClass());
		underTest.addListener(runListener1);
		underTest.addListener(runListener2);
	}

	@Test
	public void testGetListeners() {
		// given
		List<ClassRunListener> expected = Arrays.asList(runListener1,
				runListener2);
		// when
		List<ClassRunListener> actual = underTest.getListeners();
		// then
		assertEquals(expected, actual);
	}

	@Test
	public void testNotifyBeforeClass() {
		// given

		// when
		underTest.notifyBeforeClass();
		// then
		verify(runListener1).beforeClass(getClass());
		verify(runListener2).beforeClass(getClass());
	}

	@Test
	public void testNotifyAfterClass() {
		// given

		// when
		underTest.notifyAfterClass();
		// then
		verify(runListener1).afterClass(getClass());
		verify(runListener2).afterClass(getClass());
	}

	@Test
	public void testNotifyBeforeTest() {
		// given

		// when
		underTest.notifyBeforeTest(description);
		// then
		verify(runListener1).beforeTest(description);
		verify(runListener2).beforeTest(description);
	}

	@Test
	public void testNotifyAfterTest() {
		// given

		// when
		underTest.notifyAfterTest(description);
		// then
		verify(runListener1).afterTest(description);
		verify(runListener2).afterTest(description);
	}

}
