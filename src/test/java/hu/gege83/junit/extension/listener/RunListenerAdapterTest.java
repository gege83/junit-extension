package hu.gege83.junit.extension.listener;

import static org.mockito.Mockito.verify;
import hu.gege83.junit.extension.runner.ClassRunNotifier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RunListenerAdapterTest {

	private RunListenerAdapter underTest;

	@Mock
	private ClassRunNotifier classNotifier;
	@Mock
	private Description description;

	@Before
	public void setUp() throws Exception {
		underTest = new RunListenerAdapter(classNotifier);
	}

	@Test
	public void testTestStarted() throws Exception {
		// given
		// when
		underTest.testStarted(description);
		// then
		verify(classNotifier).notifyBeforeTest(description);
	}

	@Test
	public void testTestFinished() throws Exception {
		// given
		// when
		underTest.testFinished(description);
		// then
		verify(classNotifier).notifyAfterTest(description);
	}
}
