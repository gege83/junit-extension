package hu.gege83.junit.extension.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hu.gege83.junit.extension.stub.AnnotatedTestClass;
import hu.gege83.junit.extension.stub.NotAnnotatedTestClass;
import hu.gege83.junit.extension.stub.TestRunListener;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ClassRunListenerFactoryTest {

	private ClassRunListenerFactory underTest;

	@Before
	public void setUp() throws Exception {
		underTest = new ClassRunListenerFactory();
	}

	@Test
	public void testGetInstance() {
		// given
		// when
		ClassRunListenerFactory actual = ClassRunListenerFactory.getInstance();
		// then
		assertTrue(actual instanceof ClassRunListenerFactory);
	}

	@Test
	public void testGetListenresIfAnnotated() {
		// given
		List<ClassRunListener> expected = Arrays
				.asList((ClassRunListener) new TestRunListener());
		// when
		List<ClassRunListener> actual = underTest
				.getListenres(AnnotatedTestClass.class);
		// then
		assertEquals(expected, actual);
	}

	@Test
	public void testGetListenresIfNotAnnotated() {
		// given
		List<ClassRunListener> expected = Arrays.asList();
		// when
		List<ClassRunListener> actual = underTest
				.getListenres(NotAnnotatedTestClass.class);
		// then
		assertEquals(expected, actual);
	}

}
