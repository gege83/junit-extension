package hu.gege83.junit.extension.stub;

import hu.gege83.junit.extension.listener.ClassRunListener;

public class TestRunListener extends ClassRunListener {
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TestRunListener) {
			return true;
		}
		return super.equals(obj);
	}
}