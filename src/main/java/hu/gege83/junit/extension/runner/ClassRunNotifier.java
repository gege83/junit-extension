package hu.gege83.junit.extension.runner;

import hu.gege83.junit.extension.listener.ClassRunListener;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.Description;

/**
 * This class responsible to hold and notify listeners.
 */
public class ClassRunNotifier {
	private final Class<?> klass;
	private final List<ClassRunListener> listeners = new ArrayList<ClassRunListener>();

	public ClassRunNotifier(Class<?> klass) {
		this.klass = klass;
	}

	public void addListener(ClassRunListener runListener) {
		listeners.add(runListener);
	}

	public List<ClassRunListener> getListeners() {
		return listeners;
	}

	public void notifyBeforeClass() {
		for (ClassRunListener listener : listeners) {
			listener.beforeClass(klass);
		}
	}

	public void notifyAfterClass() {
		for (ClassRunListener listener : listeners) {
			listener.afterClass(klass);
		}
	}

	public void notifyAfterTest(Description description) {
		for (ClassRunListener listener : listeners) {
			listener.afterTest(description);
		}
	}

	public void notifyBeforeTest(Description description) {
		for (ClassRunListener listener : listeners) {
			listener.beforeTest(description);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (klass == null ? 0 : klass.hashCode());
		result = prime * result
				+ (listeners == null ? 0 : listeners.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClassRunNotifier other = (ClassRunNotifier) obj;
		if (klass == null) {
			if (other.klass != null) {
				return false;
			}
		} else if (!klass.equals(other.klass)) {
			return false;
		}
		if (listeners == null) {
			if (other.listeners != null) {
				return false;
			}
		} else if (!listeners.equals(other.listeners)) {
			return false;
		}
		return true;
	}

}
