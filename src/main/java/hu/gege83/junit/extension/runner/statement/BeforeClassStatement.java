package hu.gege83.junit.extension.runner.statement;

import hu.gege83.junit.extension.runner.ClassRunNotifier;

import org.junit.runners.model.Statement;

/**
 * This class extends {@link Statement}. This statement evaluate before other
 * statements.
 */
public class BeforeClassStatement extends Statement {

	private final Statement nextStatement;
	private final ClassRunNotifier notifier;

	/**
	 * Constructor
	 * 
	 * @param nextStatement
	 *            statement will run after this statement
	 * @param classNotifier
	 *            to notify before class
	 */
	public BeforeClassStatement(Statement nextStatement,
			ClassRunNotifier notifier) {
		this.nextStatement = nextStatement;
		this.notifier = notifier;
	}

	/**
	 * Overrides {@link Statement#evaluate()} to call
	 * {@link ClassRunNotifier#notifyBeforeClass()}
	 * 
	 */
	@Override
	public void evaluate() throws Throwable {
		getNotifier().notifyBeforeClass();
		getNextStatement().evaluate();
	}

	public Statement getNextStatement() {
		return nextStatement;
	}

	public ClassRunNotifier getNotifier() {
		return notifier;
	}

}
