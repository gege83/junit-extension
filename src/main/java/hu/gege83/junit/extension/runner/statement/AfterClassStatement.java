package hu.gege83.junit.extension.runner.statement;

import hu.gege83.junit.extension.runner.ClassRunNotifier;

import org.junit.runners.model.Statement;

/**
 * This class extends {@link Statement}. This statement evaluate after other
 * statements.
 */
public class AfterClassStatement extends Statement {

	private final Statement previousStatement;
	private final ClassRunNotifier classNotifier;

	/**
	 * Constructor
	 * 
	 * @param perviousStatement
	 *            statement will run before this statement
	 * @param classNotifier
	 *            to notify after class
	 */
	public AfterClassStatement(Statement perviousStatement,
			ClassRunNotifier classNotifier) {
		this.previousStatement = perviousStatement;
		this.classNotifier = classNotifier;
	}

	/**
	 * Overrides {@link Statement#evaluate()} to call
	 * {@link ClassRunNotifier#notifyAfterClass()}
	 */
	@Override
	public void evaluate() throws Throwable {
		getPreviousStatement().evaluate();
		getNotifier().notifyAfterClass();
	}

	public ClassRunNotifier getNotifier() {
		return classNotifier;
	}

	public Statement getPreviousStatement() {
		return previousStatement;
	}

}
