package hu.gege83.junit.extension.runner;

import hu.gege83.junit.extension.listener.ClassRunListener;
import hu.gege83.junit.extension.listener.ClassRunListenerFactory;
import hu.gege83.junit.extension.listener.RunListenerAdapter;
import hu.gege83.junit.extension.runner.statement.AfterClassStatement;
import hu.gege83.junit.extension.runner.statement.BeforeClassStatement;

import java.util.List;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * {@link JunitListenerRunner} is a runner to use
 * {@link hu.gege83.junit.extension.annotation.Listeners} annotation.
 */
public class JunitListenerRunner extends BlockJUnit4ClassRunner {

	private final ClassRunNotifier classRunNotifier;

	/**
	 * Constructor. As it is {@link BlockJUnit4ClassRunner} it need to have the
	 * test class. Constructor creates {@link ClassRunNotifier} and fills with
	 * {@link ClassRunListener}-s from test class.
	 * 
	 * @param klass
	 *            test class
	 * @throws InitializationError
	 *             if the test class is malformed.
	 */
	public JunitListenerRunner(Class<?> klass) throws InitializationError {
		super(klass);
		classRunNotifier = new ClassRunNotifier(klass);
		addListenres(getClassRunNotifier(), klass);
	}

	private void addListenres(ClassRunNotifier notifier, Class<?> klass) {
		List<ClassRunListener> listenres = ClassRunListenerFactory
				.getInstance().getListenres(klass);
		for (ClassRunListener runListener : listenres) {
			notifier.addListener(runListener);
		}
	}

	/**
	 * Returns {@link ClassRunNotifier} created in the constructor. So you can
	 * add your {@link ClassRunListener}-s
	 * 
	 * @return classRunNotifier
	 */
	public ClassRunNotifier getClassRunNotifier() {
		return classRunNotifier;
	}

	@Override
	protected Statement withBeforeClasses(Statement statement) {
		Statement nextStatement = super.withBeforeClasses(statement);
		return new BeforeClassStatement(nextStatement, classRunNotifier);
	}

	@Override
	protected Statement withAfterClasses(Statement statement) {
		Statement perviousStatement = super.withAfterClasses(statement);
		return new AfterClassStatement(perviousStatement, classRunNotifier);
	}

	@Override
	public void run(RunNotifier notifier) {
		notifier.addListener(new RunListenerAdapter(getClassRunNotifier()));
		super.run(notifier);
	}

}
