package hu.gege83.junit.extension.listener;

import org.junit.runner.Description;

/**
 * {@link ClassRunListener} is the base listener, all listener should be extend
 * this. {@link ClassRunListener} should have parameter less constructor.
 */
public class ClassRunListener {

	/**
	 * Runs before any of the tests.
	 * 
	 * @param klass
	 *            the class we will run.
	 */
	public void beforeClass(Class<?> klass) {
	}

	/**
	 * Runs after all test finished
	 * 
	 * @param klass
	 *            the class we run.
	 */
	public void afterClass(Class<?> klass) {
	}

	/**
	 * this will run before the test
	 * 
	 * @param description
	 *            the test case {@link Description}
	 */
	public void beforeTest(Description description) {
	}

	/**
	 * this will run after the test
	 * 
	 * @param description
	 *            the test case {@link Description}
	 */
	public void afterTest(Description description) {
	}
}
