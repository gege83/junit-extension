package hu.gege83.junit.extension.listener;

import hu.gege83.junit.extension.runner.ClassRunNotifier;

import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

/**
 * This is a class is responsible for run before and after test.
 * {@link hu.gege83.junit.extension.runner.JunitListenerRunner} creates instance
 * when
 * {@link hu.gege83.junit.extension.runner.JunitListenerRunner#run(org.junit.runner.notification.RunNotifier)}
 * called. This new instance will added as listener to the
 * {@link org.junit.runner.notification.RunNotifier}
 */
public class RunListenerAdapter extends RunListener {

	private final ClassRunNotifier classNotifier;

	/**
	 * Constructor
	 * 
	 * @param classNotifier
	 *            instance contains the listeners which are described in the
	 *            {@link hu.gege83.junit.extension.annotation.Listeners}
	 */
	public RunListenerAdapter(ClassRunNotifier classNotifier) {
		this.classNotifier = classNotifier;
	}

	/**
	 * Calls {@link ClassRunNotifier#notifyBeforeTest(Description)}<br/>
	 * {@inheritDoc}
	 */
	@Override
	public void testStarted(Description description) throws Exception {
		classNotifier.notifyBeforeTest(description);
	}

	/**
	 * Calls {@link ClassRunNotifier#notifyAfterTest(Description)}<br/>
	 * {@inheritDoc}
	 */
	@Override
	public void testFinished(Description description) throws Exception {
		classNotifier.notifyAfterTest(description);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (classNotifier == null ? 0 : classNotifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RunListenerAdapter other = (RunListenerAdapter) obj;
		if (classNotifier == null) {
			if (other.classNotifier != null) {
				return false;
			}
		} else if (!classNotifier.equals(other.classNotifier)) {
			return false;
		}
		return true;
	}

}
