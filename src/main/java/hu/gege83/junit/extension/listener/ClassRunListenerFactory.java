package hu.gege83.junit.extension.listener;

import hu.gege83.junit.extension.annotation.Listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * {@link ClassRunListenerFactory} responsible for instantiates
 * {@link ClassRunListener} which are defined in {@link Listeners} annotation.<br/>
 * {@link ClassRunListener} should have parameter less constructor.
 */
public class ClassRunListenerFactory {
	private static ClassRunListenerFactory instance;

	/**
	 * Gives back an instance of {@link ClassRunListenerFactory}
	 * 
	 * @return the same instance for multiple call.
	 */
	public static ClassRunListenerFactory getInstance() {
		if (instance == null) {
			instance = new ClassRunListenerFactory();
		}
		return instance;
	}

	/**
	 * Creates instance from {@link ClassRunListener}-s
	 * 
	 * @param klass
	 *            which run now.
	 * @return List of {@link ClassRunListener}
	 * @throws RuntimeException
	 *             if {@link ClassRunListener} cannot instantiates
	 */
	public List<ClassRunListener> getListenres(Class<?> klass) {
		List<ClassRunListener> list = new ArrayList<ClassRunListener>();
		List<Class<? extends ClassRunListener>> listenerClasses = getListenreClasses(klass);
		for (Class<? extends ClassRunListener> listenerClass : listenerClasses) {
			try {
				list.add(listenerClass.newInstance());
			} catch (IllegalAccessException | InstantiationException e) {
				throw new RuntimeException("Listener cannot instantine: "
						+ listenerClass.getCanonicalName(), e);
			}
		}
		return list;
	}

	private List<Class<? extends ClassRunListener>> getListenreClasses(
			Class<?> klass) {
		Listeners annotation = klass.getAnnotation(Listeners.class);
		List<Class<? extends ClassRunListener>> list = new ArrayList<Class<? extends ClassRunListener>>();
		if (annotation != null) {
			list.addAll(Arrays.asList(annotation.value()));
		}
		return list;
	}
}
