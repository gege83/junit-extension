package hu.gege83.junit.extension.annotation;

import hu.gege83.junit.extension.listener.ClassRunListener;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to define listeners for a test class
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Listeners {
	Class<? extends ClassRunListener>[] value();
}
